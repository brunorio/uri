var input = require('fs').readFileSync('/dev/stdin', 'utf8');
var lines = input.split('\n');

var n = parseInt(lines.shift());
var inp = 0;
var outp = 0;
for(var i = 0; i < n; i++){
    var x = parseInt(lines.shift());
    if(x >= 10 && x <= 20) inp++;
    else outp++;
}

console.log(inp + " in");
console.log(outp + " out");
//Bruno dos Santos Facebook:  https://www.facebook.com/Brunnorio
