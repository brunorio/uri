var input = require('fs').readFileSync('/dev/stdin', 'utf8');
var lines = input.split('\n');

var t = parseInt(lines.shift());
for(var i = 0; i < t; i++){
    var d = lines.shift();
    var dados = d.split(' ');
    var pa = parseInt(dados.shift());
    var pb = parseInt(dados.shift());
    var ga = parseFloat(dados.shift());
    var gb = parseFloat(dados.shift());
    var anos = 0;

    while(anos <= 100 && pa <= pb){
        pa += parseInt(pa*ga*0.01);
        pb += parseInt(pb*gb*0.01);
        anos++;
    }

    if(anos > 100) console.log("Mais de 1 seculo.");
    else console.log(anos + " anos.");
}

//Bruno dos Santos Facebook:  https://www.facebook.com/Brunnorio
