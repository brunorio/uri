var input = require('fs').readFileSync('/dev/stdin', 'utf8');
var lines = input.split('\n');

var linha = parseInt(lines.shift());
var operacao = lines.shift();

for(var i = 0; i < linha*12; i++) lines.shift();

var somatorio = 0;
for(var i = 0; i < 12; i++) somatorio += parseFloat(lines.shift());

if(operacao === 'S') console.log(somatorio.toFixed(1));
else console.log((somatorio/12).toFixed(1));
//Bruno dos Santos Facebook:  https://www.facebook.com/Brunnorio
