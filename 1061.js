var input = require('fs').readFileSync('/dev/stdin', 'utf8');
var lines = input.split('\n');

var aux = lines.shift();
aux = aux.split(" ");
var inicio = parseInt(aux[1])*86400;

aux = lines.shift();
aux = aux.split(" ");

inicio += parseInt(aux[0])*3600;
inicio += parseInt(aux[2])*60;
inicio += parseInt(aux[4]);

aux = lines.shift();
aux = aux.split(" ");
var fim = parseInt(aux[1])*86400;

aux = lines.shift();
aux = aux.split(" ");

fim += parseInt(aux[0])*3600;
fim += parseInt(aux[2])*60;
fim += parseInt(aux[4]);

var result = fim - inicio;

console.log(parseInt((result/86400)) + " dia(s)");
result = result%86400;
console.log(parseInt((result/3600)) + " hora(s)");
result = result%3600;
console.log(parseInt((result/60)) + " minuto(s)");
result = result%60;
console.log(result + " segundo(s)");

//Bruno dos Santos Facebook:  https://www.facebook.com/Brunnorio
