var input = require('fs').readFileSync('/dev/stdin', 'utf8');
var lines = input.split('\n');

var t = parseInt(lines.shift());
var t2 = parseInt(lines.shift());

if(t > t2){
    var x = t2;
    var y = t;
}
else{
    var x = t;
    var y = t2;
}

var soma = 0;
for(var i = x; i <= y; i++){
    if(i % 13 !== 0) soma += i;
}

console.log(soma);
//Bruno dos Santos Facebook:  https://www.facebook.com/Brunnorio
