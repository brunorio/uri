var input = require('fs').readFileSync('/dev/stdin', 'utf8');
var lines = input.split('\n');

var n = parseInt(lines.shift());
var coelho = 0;
var sapo = 0;
var rato = 0;
var cobaias;
for(var i = 0; i < n; i++){
    cobaias = lines.shift();
    cobaias = cobaias.split(' ');
    switch(cobaias[1]){
        case 'C': coelho += parseInt(cobaias[0]); break;
        case 'R': rato += parseInt(cobaias[0]); break;
        case 'S': sapo += parseInt(cobaias[0]); break;
    }
}

var total = coelho + rato + sapo;
console.log("Total: " + total + " cobaias");
console.log("Total de coelhos: " + coelho);
console.log("Total de ratos: " + rato);
console.log("Total de sapos: " + sapo);
console.log("Percentual de coelhos: " + (coelho/total*100).toFixed(2) + " %");
console.log("Percentual de ratos: " + (rato/total*100).toFixed(2) + " %");
console.log("Percentual de sapos: " + (sapo/total*100).toFixed(2) + " %");

//Bruno dos Santos Facebook:  https://www.facebook.com/Brunnorio
