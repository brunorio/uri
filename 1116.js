var input = require('fs').readFileSync('/dev/stdin', 'utf8');
var lines = input.split('\n');

var n = parseInt(lines.shift());

for(var i = 0; i < n; i++){
    var leitura = lines.shift();
    leitura = leitura.split(' ');
    var x = parseInt(leitura.shift());
    var y = parseInt(leitura.shift());

    if(y === 0) console.log("divisao impossivel");
    else console.log((x/y).toFixed(1));
}
//Bruno dos Santos Facebook:  https://www.facebook.com/Brunnorio
