var input = require('fs').readFileSync('/dev/stdin', 'utf8');
var lines = input.split('\n');

var n = parseInt(lines.shift());

for(var i = 0; i < n; i++){
    var x = parseInt(lines.shift());
    var string = "";
    if(x === 0) string = "NULL";
    else if(x%2 === 0) string = "EVEN";
    else string = "ODD";

    if(x > 0) string += " POSITIVE";
    else if (x < 0) string += " NEGATIVE";

    console.log(string);
}
//Bruno dos Santos Facebook:  https://www.facebook.com/Brunnorio
