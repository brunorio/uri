var input = require('fs').readFileSync('/dev/stdin', 'utf8');
var lines = input.split('\n');

var cont = 0;
var valores = 0;
var vet = new Array(6);
vet[0] = parseFloat(lines.shift());
vet[1] = parseFloat(lines.shift());
vet[2] = parseFloat(lines.shift());
vet[3] = parseFloat(lines.shift());
vet[4] = parseFloat(lines.shift());
vet[5] = parseFloat(lines.shift());

for(var i = 0; i < 6; i++){
    if(vet[i] > 0){
        cont++;
        valores += vet[i];
    }
}

console.log(cont + " valores positivos");
console.log((valores/cont).toFixed(1));

//Bruno dos Santos Facebook:  https://www.facebook.com/Brunnorio
