var input = require('fs').readFileSync('/dev/stdin', 'utf8');
var lines = input.split('\n');

var countPar = 0;
var countImpar = 0;
var par = [5];
var impar = [5];
var count = 15;
while(count !== 0){
  let value = parseInt(lines.shift());
  count--;
  if(Math.abs(value) % 2 === 0){
    if(countPar === 5){
      for(var i = 0; i < 5; i++){
        console.log("par[" + i + "] = " + par[i]);
      }
      countPar = 0;
    }
    par[countPar] = value;
    countPar++;
  }
  else{
    if(countImpar === 5){
        for(var i = 0; i < 5; i++){
          console.log("impar[" + i + "] = " + impar[i]);
        }
        countImpar = 0;
    }
    impar[countImpar] = value;
    countImpar++;
  }
}

for(var i = 0; i < countImpar; i++) console.log("impar[" + i + "] = " + impar[i]);
for(var i = 0; i < countPar; i++) console.log("par[" + i + "] = " + par[i]);

//Bruno dos Santos Facebook:  https://www.facebook.com/Brunnorio
