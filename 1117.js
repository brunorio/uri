var input = require('fs').readFileSync('/dev/stdin', 'utf8');
var lines = input.split('\n');

var i = 0;
var media = 0;
while(i < 2){
    var nota = parseFloat(lines.shift());
    if(nota >= 0 && nota <= 10){
        media += nota;
        i++;
    }
    else console.log("nota invalida");
}

console.log("media = " + (media/2).toFixed(2));
//Bruno dos Santos Facebook:  https://www.facebook.com/Brunnorio
