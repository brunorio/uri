var input = require('fs').readFileSync('/dev/stdin', 'utf8');
var lines = input.split('\n');

var frac = 0;
for(var i = 0; i <= 2; i = i + 0.2){
	if(frac == 1) frac = 0;
    for(var j = 1; j < 4; j++) {
    	if(frac === 0) console.log("I=" + i.toFixed(0) + " J=" + (j + i).toFixed(0));
    	else console.log("I=" + i.toFixed(1) + " J=" + (j + i).toFixed(1));
    }
    frac += 0.2;

}

//Bruno dos Santos Facebook:  https://www.facebook.com/Brunnorio
