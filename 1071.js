var input = require('fs').readFileSync('/dev/stdin', 'utf8');
var lines = input.split('\n');

var x = parseInt(lines.shift());
var y = parseInt(lines.shift());
var somatorio = 0;
if(x < y){
    if(x%2 === 0) x++;
    else x += 2;
    for(var i = x; i < y; i += 2) somatorio += i;
}
else{
    if(y%2 === 0) y++;
    else y += 2;
    for(var i = y; i < x; i += 2) somatorio += i;
}

console.log(somatorio);
//Bruno dos Santos Facebook:  https://www.facebook.com/Brunnorio
