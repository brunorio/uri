var input = require('fs').readFileSync('/dev/stdin', 'utf8');
var lines = input.split('\n');

var contPar = 0;
var contImpar = 0;
var contPos = 0;
var contNeg = 0;

var vet = new Array(5);
vet[0] = parseFloat(lines.shift());
vet[1] = parseFloat(lines.shift());
vet[2] = parseFloat(lines.shift());
vet[3] = parseFloat(lines.shift());
vet[4] = parseFloat(lines.shift());

for(var i = 0; i < 5; i++){
    if(vet[i]%2 === 0) contPar++;
    else contImpar++;

    if(vet[i] > 0) contPos++;
    else if (vet[i] < 0) contNeg++;
}

console.log(contPar + " valor(es) par(es)");
console.log(contImpar + " valor(es) impar(es)");
console.log(contPos + " valor(es) positivo(s)");
console.log(contNeg + " valor(es) negativo(s)");

//Bruno dos Santos Facebook:  https://www.facebook.com/Brunnorio
