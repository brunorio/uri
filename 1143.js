var input = require('fs').readFileSync('/dev/stdin', 'utf8');
var lines = input.split('\n');

var n = parseInt(lines.shift());
var mul = 1;
for(var i = 0; i < n; i++, mul++){
    console.log(mul + " " + Math.pow(mul, 2) + " " + Math.pow(mul, 3));
}

//Bruno dos Santos Facebook:  https://www.facebook.com/Brunnorio
