var input = require('fs').readFileSync('/dev/stdin', 'utf8');
var lines = input.split('\n');

var fib = [60];
fib[0] = 0;
fib[1] = 1;

for(var i = 2; i <= 60; i++) fib[i] = fib[i-1] + fib[i-2];

var n = parseInt(lines.shift());

for(var i = 0; i < n; i++){
    var ind = parseInt(lines.shift());
    console.log("Fib(" + ind + ") = " + fib[ind]);
}

//Bruno dos Santos Facebook:  https://www.facebook.com/Brunnorio
