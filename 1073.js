var input = require('fs').readFileSync('/dev/stdin', 'utf8');
var lines = input.split('\n');

var n = parseInt(lines.shift());

for(var i = 2; i <= n; i += 2) console.log(i + "^2" + " = " + Math.pow(i, 2));
//Bruno dos Santos Facebook:  https://www.facebook.com/Brunnorio
