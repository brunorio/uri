#include <stdio.h>

int main() {

    double vetor[100], valor;
	int i;
	scanf("%lf", &valor);
	vetor[0] = valor;
	for(i = 1; i < 100; i++){
		valor /= 2;
		vetor[i] = valor;
	}

	for(i = 0; i < 100; i++) printf("N[%i] = %.4lf\n", i, vetor[i]);

    return 0;
}

//Bruno dos Santos Facebook:  https://www.facebook.com/Brunnorio
