var input = require('fs').readFileSync('/dev/stdin', 'utf8');
var lines = input.split('\n');

var renda = parseFloat(lines.shift());
var imposto;
if(renda < 2000) console.log("Isento");
else{
    if(renda > 4500) imposto = 80 + 270 + (renda - 4500)*0.28;
    else if(renda > 3000) imposto = 80 + (renda - 3000)*0.18;
    else imposto = (renda - 2000)*0.08;

    console.log("R$ " + imposto.toFixed(2));
}

//Bruno dos Santos Facebook:  https://www.facebook.com/Brunnorio
