var input = require('fs').readFileSync('/dev/stdin', 'utf8');
var lines = input.split('\n');
var n = parseInt(lines.shift());
var cont = 0;
var idade = 0;
while(n > 0){
    idade += n;
    cont++;
    n = parseInt(lines.shift());
}

console.log((idade/cont).toFixed(2));

//Bruno dos Santos Facebook:  https://www.facebook.com/Brunnorio
