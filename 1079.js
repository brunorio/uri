var input = require('fs').readFileSync('/dev/stdin', 'utf8');
var lines = input.split('\n');

var n = parseInt(lines.shift());

for(var i = 0; i < n; i++){
    var result = lines.shift();
    result = result.split(' ');
    var media = 0;
    for(var j = 0; j < 3; j++){
        switch(j){
            case 0: media += parseFloat(result[j])*2; break;
            case 1: media += parseFloat(result[j])*3; break;
            case 2: media += parseFloat(result[j])*5; break;
        }
    }
    media /= 10;
    console.log(media.toFixed(1));
}

//Bruno dos Santos Facebook:  https://www.facebook.com/Brunnorio
