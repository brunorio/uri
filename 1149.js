var input = require('fs').readFileSync('/dev/stdin', 'utf8');
var lines = input.split('\n');
var result = lines.shift();
result = result.split(' ');

var a = parseInt(result.shift());
var n = parseInt(result.shift());

while(n < 0 || n === 0) n = parseInt(result.shift());

var soma = 0;

for(var i = 0; i < n; i++) soma += a + i;

console.log(soma);

//Bruno dos Santos Facebook:  https://www.facebook.com/Brunnorio
