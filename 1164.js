var input = require('fs').readFileSync('/dev/stdin', 'utf8');
var lines = input.split('\n');

var n = parseInt(lines.shift());

while(n > 0){
    var soma = 0;
    var x = parseInt(lines.shift());
    for(var j = 1; j < x; j++){
        if(x % j === 0) soma += j;
    }

    if(x == soma) console.log(x + " eh perfeito");
    else console.log(x + " nao eh perfeito");

    n--;
}

//Bruno dos Santos Facebook:  https://www.facebook.com/Brunnorio
