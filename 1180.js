var input = require('fs').readFileSync('/dev/stdin', 'utf8');
var lines = input.split('\n');

var n = parseInt(lines.shift());

var vetor = input.split('\n')[1].split(' ');
var posicao = 0;
var menor = parseInt(vetor.shift());

for(i = 1; i < n; i++){
  let valor = parseInt(vetor.shift());
  if(valor < menor){
    menor = valor;
    posicao = i;
  }
}

console.log("Menor valor: " + menor);
console.log("Posicao: " + posicao);
//Bruno dos Santos Facebook:  https://www.facebook.com/Brunnorio
