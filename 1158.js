var input = require('fs').readFileSync('/dev/stdin', 'utf8');
var lines = input.split('\n');

var n = parseInt(lines.shift());

var arraySoma = new Array(n);
for(var i = 0; i < n; i++){
    var esp = lines.shift();
    esp = esp.split(' ');
    var x = parseInt(esp.shift());
    var y = parseInt(esp.shift());
    var imp = x;
    var soma = 0;
    if(imp % 2 === 0) imp++;
    for(var j = 0; j < y; j++, imp += 2) soma += imp;
    arraySoma[i] = soma;
}

for(var i = 0; i < n; i++) console.log(arraySoma[i]);

//Bruno dos Santos Facebook:  https://www.facebook.com/Brunnorio
