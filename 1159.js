var input = require('fs').readFileSync('/dev/stdin', 'utf8');
var lines = input.split('\n');

var x = parseInt(lines.shift());
while(x !== 0){
    if(x % 2 === 0) var i = x;
    else var i = x + 1;
    var soma = 0;
    for(var cont = 0; cont < 5; cont++, i += 2) soma += i;
    console.log(soma);
    x = parseInt(lines.shift());
}

//Bruno dos Santos Facebook:  https://www.facebook.com/Brunnorio
