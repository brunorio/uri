var input = require('fs').readFileSync('/dev/stdin', 'utf8');
var lines = input.split('\n');

var n = parseInt(lines.shift());
var mul = 1;
var sentinela = true;
for(var i = 0; i < 2*n; i++){
    if(sentinela){
        console.log(mul + " " + Math.pow(mul, 2) + " " + Math.pow(mul, 3));
        sentinela = false;
    }
    else{
        console.log(mul + " " + (Math.pow(mul, 2) + 1) + " " + (Math.pow(mul, 3) + 1));
        sentinela = true;
        mul++;
    }

}

//Bruno dos Santos Facebook:  https://www.facebook.com/Brunnorio
